var mongoose = require('mongoose'),
    User = require('../models/users_model').user,
    express = require('express'),
    Models = require('../models/models'),
    utility = require('../utility'),
    url = require('url'),
    jwt = require('jsonwebtoken'),
    config = require('../config/env/' + express().get('env')),
    passport = require('passport'),
    stripe = require('stripe')(config.stripeSecretKey),
    router = express.Router(),
    _ = require('lodash'),
    moment = require('moment'),
    passwordHash = require('password-hash'),
    Multipassify = require('multipassify'),
    multipassify = new Multipassify(config.shopify.multipassApiKey);

    var localJwtOpts = {
        issuer: config.jwtOpts.issuer,
        audience: config.jwtOpts.audience,
        expiresIn: config.jwtOpts.expiresIn
    };

router.post('/me', passport.authenticate('jwt', {session: false}), function (req, res) {

    User.findById(req.user._id, function (err, dbUser) {
        if (err) return res.status(400).json({success: false, error: err});
        if (!dbUser) return res.status(404).json({success: false, error: 'Not Found'});

        return res.json({
            status: true,
            user: _.omit(dbUser.toObject(), ['password', 'salt', 'hash', '__v'])
        });
    });
});

router.put('/me/paymentMethods', passport.authenticate('jwt', {session: false}), function (req, res) {

    var entity = req.body;
    var userId = req.user._id;

    User.findById(userId).exec(function (err, userDoc) {
        if (err) return res.status(400).json({success: false, error: err});
        if (!userDoc) return res.status(404).json({success: false, error: 'Not Found'});

        userDoc.updatedAt = new Date();

        if (userDoc.stripeCustomerId) {
            stripe.customers.retrieve(userDoc.stripeCustomerId, function (err, customer) {
                if (err) return res.status(400).json({success: false, error: err});

                stripe.customers.update(customer.id, {
                    source: entity.source
                }, function (err, customer) {
                    if (err) return res.status(400).json({success: false, error: err.message});

                    userDoc.stripeCustomerId = customer.id;

                    userDoc.save(function (err, savedDoc) {
                        if (err) return res.status(400).json({success: false, error: err});

                        return res.status(200).json({success: true});
                    });
                });

            });
        }
        else {
            return res.status(400).json({success: false, error: 'Missing existing card'});
        }
    });
});

router.post('/authorize', function (req, res) {

    // find the user
    User.findOne({email: req.body.email.toLowerCase()}, function (err, dbUser) {
        if (err) throw err;

        if (!dbUser) return res.json({
            status: false,
            message: 'Authentication failed. User not found or wrong password.'
        });
        // check if password matches
        if (!passwordHash.verify(req.body.password, dbUser.password)) {
            return res.json({status: false, message: 'Authentication failed. User not found or wrong password.'}); //ambiguous for security
        }
        else {

            var multipassData = {
                email: dbUser.email,
                first_name: dbUser.nameFirst,
                last_name: dbUser.nameLast,
                created_at: dbUser.createdAt,
                return_to: req.body.returnUrl
            };

            var multipassUrl = multipassify.generateUrl(multipassData, config.public.shopify.storeUrl);

            var token = jwt.sign({
                _id: dbUser._id,
                email: dbUser.email
            }, config.jwtOpts.secretOrKey, localJwtOpts);

            // return the information including token as JSON
            return res.status(200).json({
                url: multipassUrl,
                status: true,
                token: token,
                t: Date.now(),
                user: _.omit(dbUser.toObject(), ['password', 'salt', 'hash', '__v'])
            });
        }
    });
});

router.post('/logout', function (req, res) {
    res.status(200).json({status: true});
});

router.post('/', function (req, res) {

    var newUser = {
        nameFirst: req.body.nameFirst,
        nameLast: req.body.nameLast,
        email: req.body.email.toLowerCase(),
        password: req.body.password,
        passwordConfirm: req.body.passwordConfirm,
        optTracking: req.body.optTracking,
        optAds: req.body.optAds,
        returnUrl: req.body.returnUrl
    };

    if (newUser.nameFirst.length < 1)
        return res.status(400).json({status: false, error: 'Please enter a valid first name'});

    if (newUser.nameLast.length < 1)
        return res.status(400).json({status: false, error: 'Please enter a valid last name'});

    if (newUser.email.length < 5)
        return res.status(400).json({status: false, error: 'Please enter a valid email address'});

    if (newUser.password !== newUser.passwordConfirm) {
        return res.status(400).json({status: false, error: 'Passwords do not match'});
    }

    var re = new RegExp(/^[ -~]{6,32}$/); //any character

    if (!re.test(newUser.password)) {
        return res.status(400).json({
            status: false,
            error: 'Password does not meet requirement of at least 6 characters, 1 capital letter, and 1 number. Please try again.'
        });
    }

    newUser.password = passwordHash.generate(newUser.password, {algorithm: 'sha512'});

    User.create(newUser, function (err, savedUser) {
        if (err && err.code == 11000)
            return res.status(400).json({
                status: false,
                error: 'There is already an account with that email address. If you already have an account, please try logging in.'
            });
        else if (err) return res.status(400).json({status: false, error: err});

        req.app.render('emails/signup', {
            title: 'Welcome!',
            name: newUser.nameFirst + ' ' + newUser.nameLast
        }, function (err, emailHTML) {
            if (err) return;
            utility.email({
                email: savedUser.email,
                subject: 'Welcome!',
                text: emailHTML
            });
        });

        var returnUrl = newUser.returnUrl;

        var multipassData = {
            email: savedUser.email,
            first_name: newUser.nameFirst,
            last_name: newUser.nameLast,
            return_to: returnUrl
        };

        var multipassUrl = multipassify.generateUrl(multipassData, config.public.shopify.storeUrl);

        var token = jwt.sign({
            _id: savedUser._id,
            email: savedUser.email
        }, config.jwtOpts.secretOrKey, localJwtOpts);

        return res.status(200).json({
            url: multipassUrl,
            status: true,
            token: token,
            user: _.omit(savedUser.toObject(), ['password', 'salt', 'hash', '__v'])
        });
    });
});

router.post('/change_password', passport.authenticate('jwt', {session: false}), function (req, res) {

    var newPasswordData = {
        oldPassword: req.body.oldPassword,
        password: req.body.password,
        passwordConfirm: req.body.passwordConfirm
    };

    var re = new RegExp(/^[ -~]{6,32}$/);

    User.findById(req.user._id).exec(function (err, userDoc) {
        if (err) return res.status(400).json({success: false, error: err});
        if (!userDoc) return res.status(404).json({success: false, error: 'Not Found'});

        if (!passwordHash.verify(newPasswordData.oldPassword, userDoc.password))
            return res.status(400).json({
                status: false,
                error: 'Authentication failed. Wrong existing password. Please try again.'
            });

        if (!re.test(newPasswordData.password)) {
            return res.status(400).json({
                status: false,
                error: 'Password does not meet requirement of at least 6 characters, 1 capital letter, and 1 number. Please try again.'
            });
        }

        if (newPasswordData.password !== newPasswordData.passwordConfirm) {
            return res.status(400).json({status: false, error: 'Passwords do not match. Please try again.'});
        }

        userDoc.updatedAt = new Date();

        userDoc.password = passwordHash.generate(newPasswordData.password, {algorithm: 'sha512'});

        userDoc.save(function (err, savedDoc) {
            if (err) return res.status(400).json({success: false, error: err});

            return res.status(200).json({status: true});
        });
    });
});

router.post('/reset_password', function (req, res) {

    var resetUser = {
        email: req.body.email.toLowerCase()
    };

    if (!resetUser.email || resetUser.email.length < 5)
        return res.status(400).json({status: false, error: 'Please enter a valid email address'});

    User.findOne({email: resetUser.email}).exec(function (err, userDoc) {
        if (err) return res.status(400).json({success: false, error: err});
        if (!userDoc) return res.status(200).json({status: true}); //don't expose emails

        userDoc.updatedAt = new Date();

        newPassword = 'Sn' + (Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000); // Sn123456

        userDoc.password = passwordHash.generate(newPassword, {algorithm: 'sha512'});

        req.app.render('emails/reset_password', {
            title: 'Your password has been reset',
            name: userDoc.nameFirst + ' ' + userDoc.nameLast,
            email: userDoc.email,
            newPassword: newPassword
        }, function (err, emailHTML) {
            if (err) return res.status(400).json({success: false, error: err});

            utility.email({
                email: userDoc.email,
                subject: 'Your password has been reset',
                text: emailHTML
            });

            userDoc.save(function (err, savedDoc) {
                if (err) return res.status(400).json({success: false, error: err});

                return res.status(200).json({status: true});
            });
        });
    });
});

module.exports = router;
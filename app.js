var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var compression = require('compression');
var _ = require('lodash');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

var packageData = require('./package.json');
var assetData = require('./webpack-assets.json');
var User = require('./models/users_model').user;

var app = express();
require("./routes")(app);

//set configuration variable
var config = require('./config/env/' + app.get('env'));
console.log('Env: ' + app.get('env'), 'Port: ' + config.port + '/' + process.env.PORT, 'User: ' + process.env.USER, 'DB: ' + config.mongo.host);

app.set('customPort', config.port);
app.set('x-powered-by', false);
app.enable('trust proxy');

//connect to the database
var mongoose = require('mongoose');
var db = mongoose.connect(config.mongo.host, config.mongo.options);

config.jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme(config.jwtOptions.authScheme);

passport.use(new JwtStrategy(config.jwtOptions, function (jwtPayload, done) {
    return done(null, {_id: jwtPayload._id, email: jwtPayload.email});
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(compression());

app.use(favicon(__dirname + '/public/favicon.ico'));
app.use('/public', express.static(path.join(__dirname, 'public')));

app.use(logger(app.get('env') === 'development' ? 'dev' : 'short'));

app.use(passport.initialize());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/*', function (req, res) {

    if (config.offline) {
        return res.redirect('https://' + config.public.shopify.storeUrl);
    }

    if (config.public.debug) {
        console.log('new json');
        delete require.cache[require.resolve('./webpack-assets.json')];
        assetData = require('./webpack-assets.json');
    }

    res.render('public', {publicConfig: config.public, assetData: assetData});
});

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.locals.env = app.get('env');
app.locals.package = packageData;

module.exports = app;
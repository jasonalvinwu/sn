'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    timestamps = require('mongoose-timestamp')

var UserSchema = new Schema({
    nameFirst: {
        type: String,
        default: ''
    },
    nameLast: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        default: ''
    },
    password: {
        type: String,
        default: ''
    },
    isProducer: {
        type: Boolean,
        default: false
    },
    isAffiliate: {
        type: Boolean,
        default: false
    },
    isDisabled: {
        type: Boolean,
        default: false
    },
    stripeCustomerId: {
        type: String,
        default: ''
    },
    optTracking: {
        type: Boolean,
        default: false
    },
    optAds: {
        type: Boolean,
        default: false
    },
    billing: {
        type: Object,
        default: {}
    },
    id: {
        type: Schema.ObjectId,
        ref: 'id'
    }
}, {collection: 'accounts'});

UserSchema.plugin(timestamps);

exports.user = mongoose.model('User', UserSchema);
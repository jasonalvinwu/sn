//Register the controller
var users = require('./routes/users');

module.exports = function (app) {

    var api1 = '/api/v1';

    app.use(api1 + '/users', users);
};